class icis_puppet::enc {


  package {'ruby-devel': ensure => present,}
  package {'rake': ensure => 'installed', provider =>'gem',}
  package {'gcc': ensure => 'installed', }
  package {'libxml2':  ensure => 'installed', }
  package {'libxml2-devel': ensure => 'installed', }
  package {'libxslt': ensure => 'installed', }
  package {'libxslt-devel': ensure => 'installed', }
  package { 'nokogiri': ensure => '1.5.11', provider => 'gem',  }
  package { 'aws-sdk-v1': provider => 'gem', }
  file { '/etc/puppet/dynamodb_enc': ensure => directory, }
  file { '/etc/puppet/dynamodb_enc/dynamodb_enc.erb': source => 'puppet:///modules/icis_puppet/dynamodb_enc.erb', owner => puppet, }
  file { '/etc/puppet/dynamodb_enc/dynamodb_enc.rb': source => 'puppet:///modules/icis_puppet/dynamodb_enc.rb', owner => puppet, }
  file { '/etc/puppet/dynamodb_enc/config.yml': content => template('icis_puppet/dynamodb_enc_config.yml.erb'), owner => puppet, }
  ini_setting { 'node_terminus': ensure  => present, path    => '/etc/puppet/puppet.conf', section => 'master', setting => 'node_terminus', value   => 'exec', }
  ini_setting { 'external_nodes': ensure  => present, path    => '/etc/puppet/puppet.conf', section => 'master', setting => 'external_nodes', value   => '/usr/bin/env ruby /etc/puppet/dynamodb_enc/dynamodb_enc.rb', }

  Package['ruby-devel']->
  Package['rake']->
  Package['gcc']->
  Package['libxml2']->
  Package['libxml2-devel']->
  Package['libxslt']->
  Package['libxslt-devel']->
  Package['nokogiri']->
  Package['aws-sdk-v1']->
  File['/etc/puppet/dynamodb_enc']->
  File['/etc/puppet/dynamodb_enc/dynamodb_enc.erb']->
  File['/etc/puppet/dynamodb_enc/dynamodb_enc.rb']->
  File['/etc/puppet/dynamodb_enc/config.yml']->
  Ini_Setting['node_terminus']->
  Ini_Setting['external_nodes']
}