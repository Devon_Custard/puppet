class icis_puppet::environments {


  $sources = {
    'puppet' => {
      'remote'  => 'git@bitbucket.org:Devon_Custard/puppet_repository.git',
      'basedir' => '/etc/puppet/environments',
    },
    'hiera' => {
      'remote'  => 'git@bitbucket.org/Devon_Custard/hieradata_repository.git',
      'basedir' => '/etc/puppet/hieradata',
    },
  }
  $purgedirs = ['/etc/puppet/environments:/etc/puppet/hieradata']
  class { 'r10k':
    sources       => $sources
  }
  file { '/root/.ssh':
    ensure => 'directory',
  }


#basemodulepath contains GLOBAL modules. i.e. available to any manifest irrespective of environment
  $basemodulepath  = '/etc/puppet/modules'
#environmentpath points to the dynamic environments folder.
  $environmentpath = '/etc/puppet/environments'

  ini_setting { 'environmentpath':
    ensure  => present,
    path    => '/etc/puppet/puppet.conf',
    section => 'main',
    setting => 'environmentpath',
    value   => $environmentpath
  }
  ini_setting { 'basemodulepath':
    ensure  => present,
    path    => '/etc/puppet/puppet.conf',
    section => 'main',
    setting => 'basemodulepath',
    value   => $basemodulepath
  }


  ###############NEED TO DO HIERADATA HERE!!!!!
  ###############Need to Copy R10k ssh key to ~/.ssh




  Class['r10k']->File['/root/.ssh']->Ini_Setting['environmentpath']->Ini_Setting['basemodulepath']



}