class icis_puppet::puppetmaster {

  ini_setting { 'warnings': ensure => present, path => '/etc/puppet/puppet.conf', section => 'main', setting => 'disable_warnings', value => 'deprecations', }
  ini_setting { 'server': ensure => present, path => '/etc/puppet/puppet.conf', section => 'main', setting => 'server', value => "$::fqdn" }
  service { 'puppetmaster': enable => true, ensure => 'running', }

}