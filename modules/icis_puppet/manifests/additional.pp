class icis_puppet::additional {
  package { 'deep_merge':
    ensure   => 'installed',
    provider => 'gem',
  }
  package { 'wget':
    ensure => 'installed',
  }
  package { 'hiera-eyaml':
    ensure   => 'installed',
    provider => 'gem',
  }
  package { 'highline':
    ensure   => '1.6.19',
    provider => 'gem',

  }

}