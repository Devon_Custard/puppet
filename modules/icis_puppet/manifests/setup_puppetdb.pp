class icis_puppet::setup_puppetdb {
  class {'::puppetdb': }
  class {'::puppetdb::master::config':
    manage_storeconfigs     => true,
    manage_report_processor => true,
    enable_reports          => true
  }
}