require 'rubygems'
require 'erb'
require 'aws-sdk-v1'

AWS.config(YAML.load(File.read('/etc/puppet/dynamodb_enc/config.yml')))

ddb = AWS::DynamoDB.new
table = ddb.tables['Puppet_ENC']
table.hash_key = { :fqdn => :string }
item = table.items["#{ARGV[0]}"]

environment = item.attributes[:environment]
classes = item.attributes[:classes]

template = ERB.new File.new('/etc/puppet/dynamodb_enc/dynamodb_enc.erb').read
puts template.result
