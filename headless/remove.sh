#!/bin/bash
#gem uninstall -x passenger
#gem uninstall -x rack
#gem uninstall -x rake
yum remove -y puppet rubygems hiera httpd gcc-c++ zlib-devel
rm -r -f /etc/puppet
rm -r -f /etc/httpd
rm -r -f /usr/share/puppet
rm -r -f /usr/share/puppet-dashboard
rm -r -f /var/lib/puppet
rm -r -r /var/lib/puppetdb/ssl

#rm -r -f /pin

