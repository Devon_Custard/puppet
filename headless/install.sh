#!/bin/bash

#Install repos and download packages
rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-el-6.noarch.rpm

yum install -y puppet puppet-server

#create folder to store modules
if [ -d "/pin" ]; then
  rm -r -f /pin
fi

mkdir /pin
mkdir /pin/modules

#download modules from puppet forge and from git repo
cp -r /git/puppet/modules /pin

#base puppet install + apache + passenger
puppet module install puppetlabs-inifile --modulepath=/pin/modules
puppet apply --modulepath=/pin/modules ./puppetmaster.pp --hiera_config=/git/puppet/headless/hiera.yaml

#configure dirctory environments and r10k
puppet module install zack-r10k --modulepath=/pin/modules
puppet apply --modulepath=/pin/modules ./environments.pp --hiera_config=/git/puppet/headless/hiera.yaml

#configure puppetdb
puppet module install puppetlabs-puppetdb --modulepath=/pin/modules --version=4.3.0
puppet apply --modulepath=/pin/modules ./puppetdb.pp --hiera_config=/git/puppet/headless/hiera.yaml

#configure enc
puppet apply --modulepath=/pin/modules ./enc.pp --hiera_config=/git/puppet/headless/hiera.yaml

#additional options
puppet apply --modulepath=/pin/modules ./additional.pp --hiera_config=/git/puppet/headless/hiera.yaml
